const express = require('express')

const userController = require('../Controller/user')
const checkAuth = require('../Middleware/checkAuth')

router = express.Router();

router.post("/signup", userController.signup);

router.post("/login", userController.login);

router.get("/getUser/:emailId", checkAuth, userController.getUser);

router.put("/updateUser/:emailId", checkAuth, userController.updateUser);

router.put("/addAddress/:emailId", checkAuth, userController.addAddress)

router.delete("/deleteAddress/:emailId", checkAuth, userController.deleteAddress)

module.exports = router;