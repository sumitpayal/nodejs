const jwt = require("jsonwebtoken");
const dotenv = require('dotenv');
dotenv.config();

module.exports = async (req, res, next) => {
  try {
    const token = req.body.token;
    const decodedToken = await jwt.verify(token, process.env.JWT_KEY); 
    if(!decodedToken){
      throw new Error(process.env.NOT_AUTHENTICATED);
    }
    next();
  } 
  catch ( error ) {
    res.status(401).json({
      message: process.env.AUTH_FAILED
    })
  }
}  