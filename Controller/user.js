const bcrypt = require('bcrypt')
const User = require('../Model/user')
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken')
dotenv.config()


exports.signup = async(req,res,next) => {
    const hash = await bcrypt.hash(req.body.password, 7);
         User.findOne({ email: req.body.email})
         .exec()
         .then(user =>{
            if(!user){
                newUser = new User({
                    name: req.body.name,
                    email: req.body.email, 
                    mobile: req.body.mobile,
                    role: req.body.role,
                    address: req.body.address,
                    password: hash 
                });
                newUser.save()
                        .then( result =>{ 
                            return res.status(201).json({
                                message: process.env.USER_ADDED
                            });
                        })
                        .catch(err=>{
                            return res.status(500).json({
                                message: process.env.UNKNOWN_ERROR
                            })
                        })
            }
            else{
                return res.status(409).json({
                    message: process.env.USER_EXISTS
                });
            }
         }) 
}

exports.login = async(req,res,next) =>{ 
    let fetchedUser;
    User.findOne({email: req.body.email})
        .then(async user=>{
            if(!user){
                return res.status(401).json({
                    message: process.env.AUTH_FAILED
                })                
            }
            fetchedUser=user;
            return await bcrypt.compare(req.body.password, user.password);
        })
        .then(result =>{
            if(!result){
                return res.status(401).json({
                    message: process.env.AUTH_FAILED
                })
            }
            else{
                const token = jwt.sign({email: fetchedUser.email},
                    process.env.JWT_KEY,
                    { expiresIn: '1h' }
                    );
                    res.status(200).json({
                    token: token,
                    expiresIn: process.env.LOGIN_TOKEN,  
                    email: fetchedUser.email,
                    message: process.env.LOGGED_IN
                    }) 
            }
        })
}

exports.getUser = (req,res,next) =>{
    User.findOne({ email: req.params.emailId})
        .then(user=>{
            if(!user){
                return res.status(401).json({
                    message: process.env.NOT_AUTHENTICATED
                })
            }
            res.status(200).json({
                name:user.name,
                email: user.email,
                mobile: user.mobile,
                address: user.address
            })
        })
        .catch(err =>{
            return res.status(500).json({
                message: process.env.UNEXPECTED_ERROR
            })
        })
}

exports.updateUser = (req,res,next)=>{
    let fetchUser;
    User.findOne({email: req.params.emailId})
        .then(user =>{
            if(!user){
                return res.status(401).json({
                    message: process.env.NOT_AUTHENTICATED
                })
            } 
            else{  
                    User.updateOne({email: req.params.emailId},
                                   { $set:
                                            { 
                                              name: req.body.name,
                                              mobile: req.body.mobile
                                            }
                                    })
                        .then(result =>{
                            return res.status(200).json({
                                message: process.env.USER_UPDATED
                            })
                        }) 
            }
        })
        .catch( (error) => {
            res.status(400).json({
                message: process.env.SIGNUP_FAILED
            })
        });
}

exports.addAddress = (req,res,next)=>{
    let email = req.params.emailId;
    User.findOne({ email: email })
        .then(user =>{
            if(!user){
                return res.status(401).json({
                    message: process.env.NOT_AUTHENTICATED
                })
            }
            else{    
                User.findOne({ email: email,address:{$in: [req.body.address]}})
                    .count()
                    .then(resultAddress =>{
                        if(!resultAddress){ 
                            User.updateOne({ email: email},{ $push :{ address: req.body.address}})
                            .then(result =>{
                                return res.status(200).json({
                                    message: process.env.USER_UPDATED
                                })
                            })
                        }  
                        else{ 
                            return res.status(409).json({
                                messgae: process.env.ADDRESS_EXISTS
                            }) 
                        }
                    })
                }
        });    
}

exports.deleteAddress = (req,res,next)=>{
    let email = req.params.emailId;
    User.findOne({ email: email })
        .then(user =>{
            if(!user){
                return res.status(401).json({
                    message: process.env.NOT_AUTHENTICATED
                })
            }
            else{
                User.findOne({ email: email,address:{$in: [req.body.address]}})
                .count()
                .then(resultAddress =>{
                    if(resultAddress){
                        User.updateOne({ email: email},{ $pull :{ address: req.body.address}})
                        .then(result =>{
                            return res.status(200).json({
                                message: process.env.USER_UPDATED
                            })
                        })
                    }
                    else{
                        return res.status(404).json({
                            message: process.env.ADDRESS_NOT_FOUND
                        })
                    }
                })
               
            }
        })
}

