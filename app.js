const express = require('express'); 
const cors = require('cors');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const path = require('path'); 
dotenv.config();

const userRoutes = require('./Routes/user'); 

const app = express();
app.use(cors()); 
mongoose.connect("mongodb://localhost:27017/NodeJS",
                { useUnifiedTopology: true,
                  useNewUrlParser: true
                }
            )
    .then(() => console.log('DB connected'))
    .catch(() => console.log('DB not connected')); 
    
app.use(bodyParser.json()); 

app.use("/user", userRoutes); 

console.log( process.env.PORT );
var PORT = process.env.PORT || 3000;

app.listen(PORT, function() {
  console.log("Server started on port",PORT);
});